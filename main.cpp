#include <iostream>

using namespace std;

const int tamanho=5;//declara quantidade de vetores
struct candidato
{
    char nome[60];
    int tempo;
};//corpo da estrutura com atributos
struct candidato candidatos[tamanho];//dar tamanho a variavel candidato
void lerEstrutura(struct candidato cand[],int tamanho) //corpo da funcao
{
    for(int i=0; i<tamanho; i++)
    {
        cout<<"nome: ";
        cin.getline(cand[i].nome,60);//comando getline aloca todo o nome a memoria
        cout<<"tempo: ";
        cin>>cand[i].tempo;
        cout<<endl;
        cin.ignore();
    }
}
void imprimirEstrutura(struct candidato cand[],int tamanho)
{
    for(int i=0; i<tamanho; i++)
    {
        cout<<"nome: "<<cand[i].nome<<endl;//comando cand[i].nome referese a variavel i e a variavel nome
        cout<<"tempo: "<<cand[i].tempo<<endl;
        cout<<endl;
    }

}

int main()
{

    lerEstrutura(candidatos,tamanho);//le a void com especificacao de caminho candidatos,tamanho
    imprimirEstrutura(candidatos,tamanho);//imprime a void com especificacao de caminho candidatos,tamanho
    return 0;
}
